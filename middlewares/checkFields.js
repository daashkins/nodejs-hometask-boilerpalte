function checkFields(param, allowedFields) {
    for (field in param) {
        if (!allowedFields.includes(field)) {
            return false;
        }
    }
    return true;
}

module.exports = {
    checkFields: checkFields
};