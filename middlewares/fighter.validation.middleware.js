const { fighter } = require('../models/fighter');
const {validateParams} = require('./validation');
const {checkFields} = require('./checkFields');
const ALLOWED_FIELDS  = ['name', 'power', 'defense','health'];

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    if (!req.body.health) {
        req.body.health = 100;
    }

    if (!checkFields(req.body, ALLOWED_FIELDS)) {
        return res.status(400).send({
            error: true,
            message: "Bad request!"
        });
    };

    let requestParams = [{
            param_key: 'power',
            required: true,
            type: 'number',
            validator_functions: [(param) => {
                return param < 100 && param >= 1
            }]
        },
        {
            param_key: 'defense',
            required: true,
            type: 'number',
            validator_functions: [(param) => {
                return param <= 10 && param >= 1
            }]
        },
        {
            param_key: 'name',
            required: true,
            type: 'string',
            validator_functions: [(param) => {
                return param.length > 1
            }]
        }
    ]
    validateParams(requestParams, req, res, next);
    }

    const updateFighterValid = (req, res, next) => {
        // TODO: Implement validatior for fighter entity during update
        if (!checkFields(req.body, ALLOWED_FIELDS)) {
            return res.status(400).send({
                error: true,
                message: "Bad request!"
            });
        };

        let requestParams = [{
                param_key: 'power',
                required: false,
                type: 'number',
                validator_functions: [(param) => {
                    return param < 100 && param >= 1
                }]
            },
            {
                param_key: 'defense',
                required: false,
                type: 'number',
                validator_functions: [(param) => {
                    return param <= 10 && param >= 1
                }]
            },
            {
                param_key: 'health',
                required: false,
                type: 'number',
                validator_functions: [(param) => {
                    return param <= 100 && param >= 0
                }]
            },
            {
                param_key: 'name',
                required: false,
                type: 'string',
                validator_functions: [(param) => {
                    return param.length > 1
                }]
            }
        ]
        validateParams(requestParams, req, res, next);
    }

    exports.createFighterValid = createFighterValid;
    exports.updateFighterValid = updateFighterValid;